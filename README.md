# 04-AliceAndBob

## `Clone` this Repository
* You should work on this project in your own branch.
* [Clone](https://confluence.atlassian.com/bitbucket/clone-a-repository-223217891.html) `this` repository to your local machine
* On your local machine, create a new [branch](https://confluence.atlassian.com/bitbucket/branching-a-repository-223217999.html), **please follow the instructions for creating a local branch!**
	* Where you see `<feature_branch>` replace that with your first name plus the first letter of your last name. So if your name is Thaneshwara you would create a new branch by using this command: `git branch thaneshwaraM`

## Alice and Bob Greeting


### **Objective:**
* Write a program which prompts the user to input his/her name.
* The program should greet users whose names are 'Alice' and 'Bob'.


### **Purpose:**
* To establish familiarity with
    * Control Flow
    * Conditionals
    * User input
    * Object instantation/declaration
    * Method invokation

### **Resources:**
* [Fundamental Programming Structures](Fundamental_Programming_Structures_in_Java.md)

### Unit Test
* No Unit Test


### Instructions
1. Understand how to get input from user
2. Create conditional to check against Alice and Bob
3. Print greeting to screen if Alice or Bob are true

